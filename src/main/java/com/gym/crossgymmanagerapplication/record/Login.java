package com.gym.crossgymmanagerapplication.record;

public record Login(
        String username,
        String password
) {
}
