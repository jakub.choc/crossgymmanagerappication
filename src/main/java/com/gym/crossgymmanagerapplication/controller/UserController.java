package com.gym.crossgymmanagerapplication.controller;

import com.gym.crossgymmanagerapplication.api.UserApiService;
import com.gym.crossgymmanagerapplication.record.Login;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequiredArgsConstructor
@RequestMapping("/user")
public class UserController {

    private final UserApiService userApiService;

    @GetMapping("/login")
    public String getLogin() {
        return "loginForm";
    }

    @PostMapping("/login")
    public String submitLogin(@ModelAttribute Login login, Model model) {

        if (userApiService.login(login.username(), login.password())) {
            model.addAttribute(userApiService.getUser(), "user");
            return "userProfile";
        }

    }


}
