package com.gym.crossgymmanagerapplication.controller;

import com.gym.crossgymmanagerapplication.api.BoxApiService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Controller;

@Controller
@RequiredArgsConstructor
public class BoxController {

    private final BoxApiService apiBoxService;

}
