package com.gym.crossgymmanagerapplication.util;

import com.gym.crossgymmanagerapplication.exception.CrossGymApiException;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class ApiClient {

    private static final String BASE_PATH = "http://localhost:8080/api";
    WebClient webClient = WebClient.create();


    public <T> ResponseEntity<T> get(String uri, Class<T> bodyClass) {
        ResponseEntity<T> response = webClient.get()
                .uri(BASE_PATH + uri)
                .accept(MediaType.APPLICATION_JSON)
                .retrieve()
                .toEntity(bodyClass)
                .onErrorReturn(ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build())
                .block();

        guardResponseOk(BASE_PATH + uri, response);
        return response;
    }

    private static <T> void guardResponseOk(String uri, ResponseEntity<T> response) {
        if (response == null) {
            throw CrossGymApiException.noResponse(uri);
        }
        if (response.getStatusCode() != HttpStatus.OK) {
            throw CrossGymApiException.fromUriAndStatusCode(uri, response.getStatusCode());
        }
    }

}
