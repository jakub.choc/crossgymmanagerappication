package com.gym.crossgymmanagerapplication.exception;

import jakarta.validation.constraints.NotNull;
import org.springframework.http.HttpStatusCode;

public class CrossGymApiException extends RuntimeException {

    public CrossGymApiException(String message) {
        super(message);
    }

    public static CrossGymApiException noResponse(@NotNull String uri) {
        return new CrossGymApiException(String.format("No response from server when calling: '%s'", uri));
    }

    public static CrossGymApiException fromUriAndStatusCode(@NotNull String uri, HttpStatusCode status) {
        return new CrossGymApiException(String.format("Something went wrong calling: '%s', HTTP status: '%s'", uri, status));
    }

}
