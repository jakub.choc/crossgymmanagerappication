package com.gym.crossgymmanagerapplication;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.servlet.SecurityAutoConfiguration;

@SpringBootApplication(exclude = { SecurityAutoConfiguration.class })
public class CrossGymManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrossGymManagerApplication.class, args);
    }

}
