package com.gym.crossgymmanagerapplication.api;

import com.gym.crossgymmanagerapplication.exception.CrossGymApiException;
import com.gym.crossgymmanagerapplication.record.Box;
import com.gym.crossgymmanagerapplication.util.ApiClient;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

@Service
@RequiredArgsConstructor
public class UserApiService {

    private final ApiClient apiClient;

    public boolean login(String email, String password) {
        ResponseEntity response = null;
        try {
            response = apiClient.get(String.format("/box/%d", id), Box.class);
        } catch (CrossGymApiException e) {
            throw CrossGymApiException.noResponse("");
        }
        if (response.getStatusCode() != HttpStatus.OK) {
            throw new NotFoundException(String.format("Character id %d not found", id));
        }
        return true;
    }
}
