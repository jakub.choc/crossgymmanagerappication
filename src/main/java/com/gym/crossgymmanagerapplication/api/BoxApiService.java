package com.gym.crossgymmanagerapplication.api;

import com.gym.crossgymmanagerapplication.exception.CrossGymApiException;
import com.gym.crossgymmanagerapplication.record.Box;
import lombok.RequiredArgsConstructor;
import com.gym.crossgymmanagerapplication.util.ApiClient;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.webjars.NotFoundException;

@Service
@RequiredArgsConstructor
public class BoxApiService {

    private final ApiClient apiClient;

    public Box getCharacterProfile(Long id) {
        ResponseEntity<Box> response = null;
        try {
            response = apiClient.get(String.format("/box/%d", id), Box.class);
        } catch (CrossGymApiException e) {
            throw CrossGymApiException.noResponse("");
        }
        Box body = response.getBody();
        if (body == null) {
            throw new NotFoundException(String.format("Character id %d not found", id));
        }
        return body;
    }

}
